# Project Plaid

Project Plaid is a hobbyist operating system and Smalltalk environment.

The goal is provide a full featured Smalltalk environment, without Linux or
an existing operating system underneath.

Initial target is RISC-V, running on qemu, barring workable single board computers
being available.

## License

Project Plaid is available under the MIT License.
