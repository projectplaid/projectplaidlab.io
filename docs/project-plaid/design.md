# Project Plaid Design

The main programming language for Project Plaid is [Ada](https://ada-lang.io/).

## Plaid

[Plaid](https://gitlab.com/projectplaid/plaid), the kernel, is designed as a microkernel, using
Andrew S. Tanenbaum's "Modern Operating Systems" book as 
a reference for implementation.

| Component | Description             |
| :-------- | :---------------------- |
| Plaid     | Kernel                  |
| Producer  | Init process            |
| Pattern   | Display server          |
| ST        | Smalltalk VM            |

## Producer

Producer will be the root task / init process for the
operating system.

This is the first process to run, and it coordinates the
rest of the system setup.

## Pattern

Pattern will be the display server for Project Plaid.
It is akin to Xorg / Wayland on a Linux system.

## ST

[ST](https://gitlab.com/projectplaid/plaidst) is the Smalltalk virtual machine and compiler for
Project Plaid.
